import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  constructor(
    private dataService: DataService
  ) { }

  date: any;
  states: any[];
  selected: any;

  stocks: any[];

  ngOnInit(): void {
    this.update();
  }

  update() {
    this.dataService.update(this.date ? new Date(this.date) : null);
    this.states = this.dataService.getStates();
    if (this.states) {
      this.select(this.states[0].movement.uid);
    }
  }

  select(uid: string) {
    this.selected = uid;
    const state = this.states.find(s => s.movement.uid == uid);
    this.stocks = state.stocks;
  }

  onChange(value: any) {
    console.log(value);
    this.select(value);
  }
}
