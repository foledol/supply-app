export class Stock {
  uid: number;
  depot: string;
  product: string;
  quantity: number;
  expiry: string;

  constructor(
    uid: number,
    depot: string,
    product: string,
    quantity: number,
    expiry: string) {
    this.uid = uid;
    this.depot = depot;
    this.product = product;
    this.quantity  = quantity;
    this.expiry  = expiry;
  }
}

export class Shipment {
  uid: number;
  depotFrom: string;
  depotTo: string;
  departureDate: string;
  product: string;
  quantity: number;
  expiry: string;

  constructor(
    uid: number,
    depotFrom: string,
    depotTo: string,
    departureDate: string,
    product: string,
    quantity: number,
    expiry: string) {
    this.uid = uid;
    this.depotFrom = depotFrom;
    this.depotTo = depotTo;
    this.departureDate = departureDate;
    this.product = product;
    this.quantity = quantity;
    this.expiry  = expiry;
  }
}

export class Movement {
  uid: number;
  date: number;
  type: number;
  shipment: Shipment;

  constructor(
    uid: number,
    date: number,
    type: number,
    shipment: Shipment) {
    this.uid = uid;
    this.date = date;
    this.type = type;
    this.shipment  = shipment;
  }
}

export class State {
  movement: Movement;
  stocks: Stock[];
}
