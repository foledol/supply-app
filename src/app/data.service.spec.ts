import { TestBed } from '@angular/core/testing';

import { DataService } from './data.service';
import {AppComponent} from './app.component';

describe('DataService', () => {
  let service: DataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be true', () => {
    expect(service.test()).toBeTrue();
  });

});
