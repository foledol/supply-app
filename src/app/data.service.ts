import { Injectable } from '@angular/core';

import * as moment from 'moment';

import * as data from './data.json';
import * as test from './test.json';

import { Stock, Shipment, Movement, State } from './types';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }

  readonly REGULAR = 'Regular';
  readonly expressDelay = 1;
  readonly regularDelay = 2;

  uid: number;
  stocks: Stock[];
  network: any[];
  date: any;
  shipments: Shipment[];
  movements: Movement[];
  states: State[];

  expected: string;

  initialize() {
    this._initialize((data as any).default, false, null);
  }

  update(date: any) {
    this._initialize((data as any).default, false, date);
  }

  _initialize(rawData: any, initStocks: boolean, date: any) {
    this.uid = 1;
    this.date = date;
    this.stocks = this._init(rawData.stocks);
    this.network = rawData.network;
    this.expected = rawData.expected;
    this.shipments = this._init(rawData.shipments);
    this.movements = this._initMovements(this.shipments, rawData.products);
    this.movements = this.movements.sort((a, b) => {
      return a.date - b.date !== 0 ? a.date - b.date : a.type - b.type;
    });
    let stocks = this.stocks;
    if (initStocks) {
      this._initStocks();
    }
    this.states = this.movements.map(movement => {
      stocks = this._updateStocks(stocks, movement);
      return {
        description: this.getDescription(movement),
        movement,
        stocks,
      };
    });
  }

  _init(items) {
    return items.map(item => {
      return {...item, uid: this.uid++};
    });
  }

  _initStocks() {
    this.shipments.forEach(shipment => {
      this.stocks.push(new Stock(shipment.uid, shipment.depotFrom, shipment.product, shipment.quantity, shipment.expiry));
    });
  }

  _initMovements(shipments, products) {
    const movements = [];
    shipments.forEach(shipment => {

      // Get the product
      const product = products.find(p => p.label === shipment.product);
      if (!product) {
        this.log(`Shipment ${shipment.uid} : invalid product`);
        return;
      }

      // Get the route
      const route = this.network.find(r => r.depotFrom === shipment.depotFrom && r.depotTo === shipment.depotTo);
      if (!route) {
        this.log(`Shipment ${shipment.uid} : invalid route`);
        return;
      }

      const departure = new Date(shipment.departureDate);
      const delay = product.shipmentType === this.REGULAR ?
        ('regularDelay' in route ? route.regularDelay : this.regularDelay) :
        ('expressDelay' in route ? route.expressDelay : this.expressDelay);
      const arrival = new Date(departure);
      arrival.setDate(arrival.getDate() + delay);

      const expiry = new Date(shipment.expiry);
      if (expiry < departure) {
        this.log(`Shipment ${shipment.uid} : product expired before departure`);
      } else if (expiry < arrival) {
        this.log(`Shipment ${shipment.uid} : product expired before arrival`);
      }

      if (!this.date || departure < this.date) {
        movements.push(new Movement(this.uid++, departure.getTime(), 0, shipment));
      }
      if (!this.date || arrival < this.date) {
        movements.push(new Movement(this.uid++, arrival.getTime(), 1, shipment));
      }
    });
    return movements;
  }

  _updateStocks(stocks, movement) {
    const shipment = movement.shipment;
    // Departure movement : remove the batch from the stock
    if (movement.type === 0) {
      return stocks.filter(stock =>
        stock.depot !== shipment.depotFrom || stock.uid !== shipment.uid
      );
    }

    // Arrival movement : add the batch to the stock
    let newStocks = [...stocks];
    newStocks.push({
      depot: shipment.depotTo,
      product: shipment.product,
      quantity: shipment.quantity,
      expiry: shipment.expiry,
      uid: shipment.uid
    });
    newStocks = newStocks.sort((a, b) => {
      return a.depot === b.depot ? new Date(a.expiry).getTime() - new Date(b.expiry).getTime() : a.depot.localeCompare(b.depot);
    });
    return newStocks;
  }

  getDescription(movement) {
    const shipment = movement.shipment;
    const date = moment(new Date(movement.date)).format('YYYY-MM-DD');
    if (movement.type === 0) {
      return `${date} departure of batch ${shipment.uid} from ${shipment.depotFrom}`;
    }
    return `${date} arrival of batch ${shipment.uid} to ${shipment.depotTo}`;
  }

  getStates() {
    return this.states;
  }

  log(message) {
    // this._logs.push(message);
    console.log(message);
  }

  test() {
    this._initialize((test as any).default, false, null);
    return JSON.stringify(this.getStates()) === JSON.stringify(this.expected);
  }
}
