# Supply Chain

The purpose of this application is to help the supply manager to predict the stocks that will be available after each shipment.

## Requirements

- Angular 9.0.3
- Moment 2.24.0

## Installation

Clone the git repository

```
git clone git@gitlab.com:foledol/supply-app.git
```

## Usage

To build the application, run

```
ng build
```

To test the application, run

```
ng test
```

To serve the application, run

```
ng serve
```

## Notes

This README file will be improved later
